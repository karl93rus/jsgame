const ctx = document.getElementById('canvas2d').getContext('2d');
const img = require( '../../img/body_03.png');

let im = new Image();
let isLoaded = false;
im.addEventListener('load', () => {
  isLoaded = true;
});
im.src = img;

class Enemy {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
  draw() {
    if(isLoaded) {
      ctx.drawImage(im, this.x - 16, this.y, 32, 64);
    }
  }
  move() {
    let min = -0.06;
    let max = 0.06;
    //this.x += Math.floor(Math.random()*(max-min+1)+min);
    this.y += Math.floor(Math.random()*(max-min+1)+min) + 0.2;
    if(this.x <= 0 || this.y <= 0) {
      this.x += 1;
      this.y += 1;
    }
  }
}

module.exports = Enemy;
