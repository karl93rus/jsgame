const GameObject = require('./GameObject.js');
const Bullet = require('./Bullet.js');
const img = require( '../../img/body_01.png');

let im = new Image();
let isLoaded = false;
im.addEventListener('load', () => {
  isLoaded = true;
});
im.src = img;

class VisibleObject extends GameObject {
  constructor(name, x, y, width, height, speed) {
    super(name);
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.dx = 10;
    this.dy = 10;
    this.speed = speed;
  }
  draw(ctx) {
    if(isLoaded) {
      ctx.drawImage(im, this.x - 16, this.y, 32, 64);
    }
  }
  move(x, y, lastx, lasty) {
    const vx = x - lastx;
    const vy = y - lasty;
    const vl = Math.sqrt(vx*vx + vy*vy);
    this.x += Math.trunc(vx) / vl * 4;
    this.y += Math.trunc(vy) / vl * 4;
  }
  moveLeft() {
    this.x -= this.speed;
  }
  moveRight() {
    this.x += this.speed;
  }
}

module.exports = VisibleObject;
