const ctx = document.getElementById('canvas2d').getContext('2d');

class Bullet {
  constructor(x, y, speed) {
    this.speed = speed;
    this.x = x;
    this.y = y;
  }
  draw() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, 3, 0, Math.PI * 2);
    ctx.fillStyle = '#aaf';
    ctx.fill();
    ctx.closePath();
  }
  move() {
    this.y -= this.speed;
  }
}

module.exports = Bullet;
