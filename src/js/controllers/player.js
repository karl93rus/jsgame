const Bullet = require('../gameObjects/Bullet.js');

class PlayerController {
  constructor() {
    this.isLeftPressed = false;
    this.isRightPressed = false;
    this.isFired = false;
  }
  move(event) {
    if(event.key == 'ArrowLeft') {
      this.isLeftPressed = true;
    } else if(event.key == 'ArrowRight') {
      this.isRightPressed = true;
    } 
  }
  stop(event) {
    if(event.key == 'ArrowRight') {
      this.isRightPressed = false;
    } else if(event.key == 'ArrowLeft') {
      this.isLeftPressed = false;
    }
  }
  fire(event) {
    console.log('isFired', this.isFired);
    if(event.key == ' ') {
      bullet = new Bullet(p.x, p.y, 6);
    }
  }
}

module.exports = PlayerController;
