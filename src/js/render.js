const Visible = require('./gameObjects/VisibleObject.js');
const Bullet = require('./gameObjects/Bullet.js');
const Enemy = require('./gameObjects/Enemy.js');
const PlayerController = require('./controllers/player.js');

let scoreSpan = document.getElementById('score');
let score = 0;
scoreSpan.textContent = score;
let player = new Visible('player', 375, 410, 10, 50, 4);
let bullets = [];
let enemies = [];
let pos = 50;
for(let i = 0; i < 10; i++) {
  enemies.push(new Enemy(pos, Math.trunc(Math.random() * 100)));
  pos += 75;
}
const controller = new PlayerController();

document.addEventListener('keydown', controller.move.bind(controller), false);
document.addEventListener('keypress', (e) => {
  if(e.key == ' ') {
    bullets.push(new Bullet(player.x, player.y, 5));
  }
}, false);
document.addEventListener('keyup', controller.stop.bind(controller), false);

class Game {
  constructor(canvas, width, height) {
    this.canvas = canvas;
    canvas.width = width;
    canvas.height = height;
    this.ctx = canvas.getContext('2d');
  }
  gameDraw() {
    player.draw(this.ctx);
    for(let b of bullets) {
      b.draw();
    }
    for(let e of enemies) {
      e.draw();
    }
  }
  gameMove() {
    if(controller.isLeftPressed) {
      player.moveLeft();
    } else if(controller.isRightPressed) {
      player.moveRight();
    }

    for(let e of enemies) {
      e.move();
      if(e.y >= (500 - 64)) {
        score -= 1;
        scoreSpan.textContent = score;
        enemies.splice(enemies.indexOf(e), 1);
        enemies.push(new Enemy(Math.trunc(Math.random() * 700), Math.trunc(Math.random() * 100) - 100));
      }
    }

    for(let b of bullets) {
      b.move();
      if(b.y < 0) {
        bullets.splice(bullets.indexOf(b), 1);
      } 
      for(let e of enemies) {
        if((b.x > e.x - 16 && b.x < e.x + 16 && b.y <= e.y + 64)) {
          bullets.splice(bullets.indexOf(b), 1);
          enemies.splice(enemies.indexOf(e), 1);
          score += 1;
          scoreSpan.textContent = score;
          enemies.push(new Enemy(Math.trunc(Math.random() * 700), Math.trunc(Math.random() * 100) - 100));
        }
      }
    }
  }
  render() {
    setInterval(() => {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

      this.gameDraw();

      this.gameMove();

    }, 16);
  }
}

module.exports = {
  Game
}
