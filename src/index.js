import BG from './img/bg.jpg';
import './css/main.css';

const Game = require('./js/render.js').Game;

const gc = document.getElementById('canvas2d');
const game = new Game(gc, 800, 500);

game.render();

